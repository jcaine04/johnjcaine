import os

class Config(object):

    UPLOAD_FOLDER = os.path.realpath('.') + '/app/o2sc/uploads/'
    DOWNLOAD_FOLDER = os.path.realpath('.') + '/app/o2sc/downloads/'


    @staticmethod
    def init_app(app):
        pass