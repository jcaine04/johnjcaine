from flask import Flask
from flask.ext.bootstrap import Bootstrap
from config import Config

bootstrap = Bootstrap()

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)
    Config.init_app(app)

    bootstrap.init_app(app)

    from main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from o2sc import o2sc as o2sc_blueprint
    app.register_blueprint(o2sc_blueprint)

    return app

app = create_app()
